from codewords import codewords, synsets
from collections import Counter
from models import *
from sklearn import feature_extraction, svm
from sklearn.lda import LDA
from sklearn.feature_extraction.text import TfidfTransformer
from spellcheck import *

import lda
import lda.datasets
import nltk
import numpy as np
import re
import sys

TRAIN_TEST_SPLIT = 1100
ENCODING_ID = 9
SCALE_POSITIVES = 20
NUM_CODES = 34
USED_IDS = [2, 16, 20, 24]

def spellcheck(phrase):
    result = []
    for word in re.findall(r"[\w']+", phrase):
        result.append(correct(word))
    return (" ").join(result)

def makeDataArrays():
    responses = []
    answers = []
    
    for question in Question.select():
        for response in question.responses():
            data = [answer.encoded for answer in response.answers()]
            #responses.append(response.text)
            responses.append(spellcheck(response.text))
            answers.append(data[ENCODING_ID])
            if data[ENCODING_ID] == True:
                for i in xrange(SCALE_POSITIVES - 1):
                    responses.append(response.text)
                    answers.append(data[ENCODING_ID])
    if len(responses) > 4000:
        responses = responses[:4000]
        answers = answers[:4000]
    train_test_split = int((len(responses) + 0.0) * 3.0 / 5.0)
    return responses, answers, train_test_split


def codeWordsFeaturizer(responses):
    cw = codewords()
    all_counts = []
    for response in enumerate(responses):
        counts = []
        for i in xrange(NUM_CODES):
            count = 0
            for word in synsets(cw[i]):
                if word in response:
                    count += 1
            counts.append(count)
        all_counts.append(counts)
    return all_counts


def tfidfFeaturizer(responses):
    '''Proof of concept featurizer. Just makes bag-of-words vectors
    for each response. Will beef up once we get more working.''' 
    transformer  = TfidfTransformer() 
    vec = feature_extraction.DictVectorizer()
    features = []
    for response in responses:
        # response_tokens = nltk.word_tokenize(response)
        # pos = ['JJ', 'NN']
        # tags = [str(tag[0]) for tag in nltk.pos_tag(response_tokens) if tag[1] in pos]
        c = Counter()
        # for word in tags:
        for word in response.split():
            c[word.rstrip()] += 1.0
        features.append(c)
    features = vec.fit_transform(features).toarray()
    return transformer.fit_transform(features).toarray(), vec.vocabulary_


def baselineFeaturizer(responses):
    '''Proof of concept featurizer. Just makes bag-of-words vectors
    for each response. Will beef up once we get more working.''' 
    vec = feature_extraction.DictVectorizer()
    features = []
    for response in responses:
        response_tokens = nltk.word_tokenize(response)
        pos = ['JJ', 'NN']
        tags = [str(tag[0]) for tag in nltk.pos_tag(response_tokens) if tag[1] in pos]
        c = Counter()
        for word in tags:
        #for word in response.split():
            c[word.rstrip()] += 1.0
        features.append(c)
    return vec.fit_transform(features).toarray(), vec.vocabulary_


def runModel(responses, answers, train_test_split, clf=LDA()):
    '''Can pass in LDA() [note this is Linear Discriminant Analysis, not
    Latent Dirichlet Analysis], svm.SVC(), or any other similary 
    structured sklearn model.'''
    features, vocab = baselineFeaturizer(responses)
   # features, vocab = tfidfFeaturizer(responses)
    #codeFeatures = codeWordsFeaturizer(responses)
    #np.concatenate((features, codeFeatures), axis=1)
    # Will switch to K-fold Cross Validation later
    train_features = features[:train_test_split]
    test_features = features[train_test_split:]
    train_answers = answers[:train_test_split]
    test_answers = answers[train_test_split:]
    if (True not in train_answers or False not in train_answers):
        print "Only one class existed for ENCODING_ID {}".format(ENCODING_ID)
        return
    sys.stderr.write("\tTraining...\n")
    clf.fit (train_features, train_answers)
    sys.stderr.write("\tTesting...\n")
    predicted = clf.predict(test_features)
    sys.stderr.write("\tEvaluating...\n")
    evaluate(predicted, test_answers)


def interpret_nzw(nzw):
    '''nzw_ is a 2D matrix assigning the features to two classes.'''
    predicted = []
    for pred in nzw:
        predicted.append(True if pred == 1 else False)
    return predicted


def runLatentDirichletAllocation(responses, answers):
    model = lda.LDA(n_topics=2, n_iter=500, random_state=1)
    features, vocab = baselineFeaturizer(responses)
    features2 = []
    for feature_list in features:
        features2.append(np.array([int(x) for x in feature_list]))
    features2 = np.array(features2)
    # Will switch to K-fold Cross Validation later
      
    features = baselineFeaturizer(responses)
    
    train_features = features2[:TRAIN_TEST_SPLIT]
    test_features = features2[TRAIN_TEST_SPLIT:]
    train_answers = answers[:TRAIN_TEST_SPLIT]
    test_answers = answers[TRAIN_TEST_SPLIT:]
    
    if (True not in train_answers or False not in train_answers):
        print "Only one class existed for ENCODING_ID {}".format(ENCODING_ID)
        return
  
    model.fit(features2)
    print "Model.nzw[0] evaluated"
    predicted = interpret_nzw(model.nzw_[0])[:2577]
    answers = answers[:2577]
    evaluate(predicted, answers)
    print "Model.nzw[1] evaluated"
    predicted = interpret_nzw(model.nzw_[1])
    evaluate(predicted, answers)


def evaluate(predicted, actual):
    tp = 0
    fp = 0 
    tn = 0
    fn = 0
    for p, a in zip(predicted, actual):
        if a:
            if p:
                tp += 1
            else:
                fn += 1
        else:
            if p:
                fp += 1
            else: 
                tn += 1

    precision = 0.0 if tp == 0 and fp == 0 else (tp + 0.0) / (tp + fp) 
    recall = 0.0 if tp == 0 and fn == 0 else (tp + 0.0) / (tp + fn)
    f1 = 0.0 if precision == 0.0 and recall == 0.0 \
            else (2.0 * precision * recall) / (precision + recall)
    
    print "tp = {}".format(tp)
    print "fp = {}".format(fp)
    print "tn = {}".format(tn)
    print "fn = {}".format(fn)
    print "Recall = {}".format(recall)
    print "Precision = {}".format(precision)
    print "F1 Score = {}".format(f1)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        ENCODING_ID = int(sys.argv[1])
    if ENCODING_ID == -1:
        for i in USED_IDS:
            print "=" * 80
            ENCODING_ID = i
            sys.stderr.write("Starting encoding_id {0}\n".format(i))
            print "ENCODING_ID {}".format(i)
            sys.stderr.write("\tGetting data arrays...\n")
            responses, answers, train_test_split = makeDataArrays()
            runModel(responses, answers, train_test_split)
            print ""
            sys.stderr.write("\tFinished running encoding_id {0}, size {1}\n\n".format(i, len(responses)))
    else:
        responses, answers, train_test_split = makeDataArrays()
        runModel(responses, answers, train_test_split)
