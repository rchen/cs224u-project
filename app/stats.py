from models import *

for question in Question.select():
  print question.text
  num_responses = 0
  len_responses = 0
  words_responses = 0
  for response in question.responses():
    num_responses += 1
    len_responses += len(response.text)
    words_responses += len(response.text.split())
  print num_responses, len_responses / num_responses, words_responses / num_responses
