from nltk.corpus import wordnet as wn

def synsets(vec):
    ret = []
    for word in vec:
        for synset in wn.synsets(word):
            for lemma in synset.lemmas():
                ret.append(lemma.name())
    ret.extend(vec)
    return list(set(ret))


def codewords():
    return [
            # General
            [],
            # Party
            ['Democrat', 'Republican', 'party'],
            # Ideology/Philosophy
            ['conservative', 'liberal', 'socialist', 'Marxist', 'communist', 'fascist', 'libertarian'],
            # Electability
            ['win', 'lose'],
            # Political experience
            ['political', 'experienced', 'inexperienced'],
            # Military experience
            ['military', 'army', 'air force', 'navy', 'marines', 'national guard'],
            # Non-political / military experience
            ['business', 'industry', 'education'],
            # Scandal / Cover-up
            ['scandal', 'cover-up'],
            # Other past activity
            [],
            # Personality - ability
            ['ability'],
            # Personality - honesty
            ['honesty', 'integrity', 'consistency', 'predictability', 'sincerity', 'truthfulness'],
            # Personality - intelligence
            ['intelligence'],
            # Personality - leadership
            ['lead', 'inspire', 'motivate'],
            # Personality - other
            [],
            # Health
            ['health'],
            # Religion
            ['religious'],
            # Education
            ['education', 'school', 'college', 'degree'],
            # Physical appearance
            ['appearance', 'attractiveness', 'looks'],
            # Demographics
            ['age', 'sex', 'orientation', 'race', 'ethnicity', 'height', 'weight', 'married'],
            # Policy - economic
            ['economy', 'taxes', 'spending', 'budget', 'deficit', 'debt'],
            # Policy - poor people
            ['poor', 'welfare', 'stamps', 'Medicaid', 'AFDC', 'public housing'],
            # Policy - liberty
            ['legal', 'illegal', 'allowed'],
            # Policy - enemy countries
            ['enemies', 'war', 'sanctions'],
            # Policy - friendly countries
            ['friends', 'allies', 'cooperation'],
            # Policy - general foreign policy
            ['foreign policy', 'reputation'],
            # Policy - other
            ['policy'],
            # Groups
            ['group'],
            # Emotions / Feelings
            ['happy', 'sad', 'angry', 'proud', 'afraid', 'scared'],
            # Candidate - other
            [],
            # Non-candidate
            [],
            # Don't know
            ['don\'t know', 'unsure'],
            # Refuse
            ['refuse'],
            # Respondent - other
            [],
            # All other
            []
            ]
