from peewee import *

db = SqliteDatabase('data.db')

class BaseModel(Model):
  class Meta:
    database = db

class Question(BaseModel):
  text = CharField()
  def codes(self):
    return (Code
            .select()
            .join(QuestionCode)
            .join(Question)
            .where(Question.id == self.id))
  def responses(self):
    return (Response
            .select()
            .join(Question)
            .where(Question.id == self.id))

class Code(BaseModel):
  name = CharField()
  description = CharField()

class Response(BaseModel):
  question = ForeignKeyField(Question, related_name='question')
  text = CharField()
  def answers(self):
    return (Answer
            .select()
            .join(Response)
            .where(Response.id == self.id))

class Answer(BaseModel):
  code = ForeignKeyField(Code, related_name='code')
  response = ForeignKeyField(Response, related_name='response')
  encoded = BooleanField()

class QuestionCode(BaseModel):
  question = ForeignKeyField(Question)
  code = ForeignKeyField(Code)

db.connect()

def main():
  db.drop_tables([Question, Code, Response, Answer, QuestionCode], safe=True)
  db.create_tables([Question, Code, Response, Answer, QuestionCode], True)

if __name__ == "__main__":
  main()
