from models import *
import csv

DATA_FILES_PREFIX = '../data/csv'

def str_to_bool(v):
  return v.lower() in ("yes", "true", "t", "1")

def import_codes(filename):
  start = -1
  end = -1
  with open(DATA_FILES_PREFIX + filename, 'rb') as csvfile:
    reader = csv.reader(csvfile)
    for i, row in enumerate(reader):
      code = Code(name=row[1], description=row[2])
      code.save()
      if i == 0:
        start = code.id
      end = code.id
  return start, end

def import_question(filename, start, end):
  with open(DATA_FILES_PREFIX + filename, 'rb') as csvfile:
    reader = csv.reader(csvfile)
    question = None
    codes = None
    for i, row in enumerate(reader):
      if i == 0:
        question = Question(text=row[0])
        question.save()
        for j in xrange(start, end+1):
          QuestionCode(question=question, code=Code.get(Code.id==j)).save()
        codes = (Question.select().join(QuestionCode).join(Code).where(Question.id == question.id))
      else:
        response = Response(question=question, text=row[1])
        response.save()
        for j, code in enumerate(codes):
          encoded = True if row[j+2] == "1" else False
          Answer(code=code, response=response, encoded=encoded).save()

data_files = {
  '/person/like_dislike_codes.csv': [
    '/person/like_obama.csv',
    '/person/dislike_obama.csv',
    '/person/like_mccain.csv',
    '/person/dislike_mccain.csv'
  ]
  #'/group/like_dislike_codes.csv': [
    # '/group/like_democrat.csv',
    # '/group/dislike_democrat.csv',
    # '/group/like_republican.csv',
    # '/group/dislike_republican.csv'
  #]
}

for codes, questions in data_files.iteritems():
  start, end = import_codes(codes)
  for question in questions:
    import_question(question, start, end)
